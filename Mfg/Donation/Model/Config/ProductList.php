<?php


namespace Mfg\Donation\Model\Config;

use Magento\Framework\Option\ArrayInterface;

class Productlist implements ArrayInterface
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $collection = $this->collectionFactory->create();
        $collection
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name');

        $ret        = [];
        foreach ($collection as $product) {
            $ret[] = [
                'value' => $product->getSku(),
                'label' => $product->getName(),
            ];
        }

        return $ret;
    }
}