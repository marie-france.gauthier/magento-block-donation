<?php 

namespace Mfg\Donation\Block;

use Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Catalog\Block\Product\ListProduct;
use \Magento\Checkout\Model\Session;

class DisplayDonation extends Template {  

    public function __construct(Context $context, 
                                array $data = [], 
                                Session $checkoutSession,
                                ProductFactory $productFactory,
                                StoreManagerInterface $storeManager,
                                ListProduct $listProductBlock
                                ) 
    {
        $this->_productFactory = $productFactory;
        $this->_storeManager = $storeManager;
        $this->listProductBlock = $listProductBlock;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        
    }

    public function getProduct($sku)
    {
        return $this->_productFactory->create()->loadByAttribute('sku', $sku);
    }

    public function getImagePathFromSku($sku){
        
		$product = $this->getProduct($sku);
        if($product) {
            $mediaurl= $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
            $imagepath = $mediaurl.'catalog/product'.$product->getImage();

            return $imagepath;
        } else {
            return;
        }
	}

    public function getAddToCartPostParams($product)
    {
        return $this->listProductBlock->getAddToCartPostParams($product);
    }

    public function getQuoteData()
    {
        $this->checkoutSession->getQuote();
        if (!$this->hasData('quote')) {
            $this->setData('quote', $this->checkoutSession->getQuote());
        }
        
        return $this->_getData('quote')->getAllVisibleItems();
    }

    public function donationProductInCart($skuProduct) 
    {
        $items = $this->getQuoteData($skuProduct);
        foreach($items as $item) {
            if($item->getSku() === $skuProduct) {
                return true;
            }
        }
        return false;
    }


    
}
?>