<?php

namespace Mfg\Donation\Plugin;

use Mfg\Donation\Helper\Data;
use \Magento\Framework\App\Request\Http;

class QuotePlugin
{
  /**
   * @param \Magento\Quote\Model\Quote $subject
   * @param \Magento\Catalog\Model\Product $product
   * @param null|float|\Magento\Framework\DataObject $request
   * @param null|string $processMode
   * @throws \Exception
   * @return array
   */

  public function __construct(Data $data, Http $request)
  {
    $this->data = $data;
    $this->request = $request;
  }
  
  public function beforeAddProduct(
    $subject,
    $product,
    $request = null,
    $processMode = \Magento\Catalog\Model\Product\Type\AbstractType::PROCESS_MODE_FULL
  )
  {      
    $skuProductDonation = $this->data->getConfig('donation/general/sku_product');
    
    if($skuProductDonation === $product->getSku()) {
      $newPrice = $this->request->getParam('amount');
      $product->setPrice($newPrice);
      $request['custom_price'] = $newPrice;
    }

    return [$product, $request, $processMode];
  }       
} 